# prometheus-in-docker-compose
## Состав проекта
* `files/applications`
    * в данном каталоге располагается docker-compose.yml для запуска стэка wordpress + mysql + prometheus exporters - далее по тексту - `приложение`
* `files/monitoring`
    * в данном каталоге располагается  docker-compose.yml для запуска стэкв мониторинга - prometheus + grafana - далее по тексту - `стэк мониторинга`
## Порядок запуска
0. Устанавливаем docker
    * `bin/docker_install_ubuntu.sh`
1. Копируем файл `bin/_env.sh.sample` в `bin/_env.sh`
2. Вносим правки в файл `bin/_env.sh` - переменная `EXPORTERS_IP` должна содержать IP машины, на которой будет запущено приложение
3. Запускаем приложение
    * `bin/start_application.sh`
4. Запускаем стэк мониторинга
    * `bin/start_monitoring.sh`
## Удаление
Для удаления стэка мониторинга и приложения вместе с их данными, выполняем:
```
bin/destroy_application.sh
bin/destroy_monitoring.sh
```
