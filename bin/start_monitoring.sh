#!/bin/bash -x

set -a;
source $( dirname -- "$BASH_SOURCE" )/_env.sh
set +a;
cat files/monitoring/files/prometheus/conf/prometheus.tpl | envsubst > files/monitoring/files/prometheus/conf/prometheus.yml;
unset $( cut -d= -f1 $( dirname -- "$BASH_SOURCE" )/_env.sh );

sudo chmod -R 777 files/monitoring/files/{prometheus,grafana}/data

docker compose \
    -f files/monitoring/docker-compose.yml \
    pull \
  && docker compose \
        -f files/monitoring/docker-compose.yml \
        build \
  && docker compose \
        -f files/monitoring/docker-compose.yml \
        -p monitoring \
        up -d
