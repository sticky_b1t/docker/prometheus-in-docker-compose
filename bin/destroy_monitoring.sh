#!/bin/bash

docker compose \
    -f files/monitoring/docker-compose.yml \
    down -v \
  && sudo rm -rf files/monitoring/files/{prometheus,grafana}/data/*
