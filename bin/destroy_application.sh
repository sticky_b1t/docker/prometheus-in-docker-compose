#!/bin/bash

docker compose \
    -f files/application/docker-compose.yml \
    down -v \
  && sudo rm -rf files/application/files/mysql/data/*
