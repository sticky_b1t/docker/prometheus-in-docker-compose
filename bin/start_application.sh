#!/bin/bash -x

set -a;
source files/application/.env;
set +a;
export APPLICATION_IPAM_SUBNET=$( echo ${APPLICATION_IPAM_SUBNET} | awk -F '.' {'print $1"."$2".%.%"'} );
cat files/application/files/mysql/initdb/exporter.tpl | envsubst > files/application/files/mysql/initdb/exporter.sql;
unset $( cut -d= -f1 files/application/.env );

source $( dirname -- "$BASH_SOURCE" )/_env.sh

docker compose \
    -f files/application/docker-compose.yml \
    pull \
  && docker compose \
        -f files/application/docker-compose.yml \
        build \
  && EXPORTERS_IP=${EXPORTERS_IP} docker compose \
        -f files/application/docker-compose.yml \
        -p application \
        up -d
