#!/bin/bash

find_exporters() {
  EXPORTERS_LIST=$( curl -s --unix-socket /var/run/docker.sock http://localhost/v1.43/containers/json \
    | jq -r '.[]|select(.Labels.is_exporter=="true")|.Labels."com.docker.compose.service" + ";" + (.Ports[0].PublicPort|tostring)' )
  if [ x"${EXPORTERS_LIST}" != x"" ];
  then
    EXPORTERS_LIST_FINAL="";
    for LINE in ${EXPORTERS_LIST};
    do
      EXPORTER_NAME=$( echo ${LINE} | awk -F ';' {'print $1'} );
      EXPORTER_PORT=$( echo ${LINE} | awk -F ';' {'print $2'} );
      EXPORTERS_LIST_FINAL="${EXPORTERS_LIST_FINAL}{\"targets\":[\"${EXPORTERS_IP}:${EXPORTER_PORT}\"],\"labels\":{\"job\":\"${EXPORTER_NAME}\"}},"
    done;
    echo "[$( echo ${EXPORTERS_LIST_FINAL} | sed 's/,$//')]"
  else
    echo "[]"
  fi;
}

while true;
do
  echo -e "HTTP/1.1 200 OK\nContent-Type: application/json\n\n$(find_exporters)" | nc -l -k -p 8081 -q 1;
done;
