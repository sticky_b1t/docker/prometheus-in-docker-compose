#!/bin/bash -x

URL="http://prometheus:9090";

STATUS_CODE=$( curl -s -o /dev/null -w '%{http_code}' "${URL}/-/ready" );
while [ ${STATUS_CODE} -ne 200 ];
do
  sleep 5;
  echo "{\"ts\":\"$( date -u +'%Y-%m-%dT%H:%M:%SZ' )\",\"level\":\"info\",\"msg\":\"Waiting for Prometheus to start...\"}";
  STATUS_CODE=$( curl -s -o /dev/null -w '%{http_code}' "${URL}/-/ready" );
done;

PROMETHEUS_CONF_CHECKSUM=$( md5sum /srv/prometheus/conf/prometheus.yml | awk {'print $1'} );
while true;
do
  PROMETHEUS_CONF_CHECKSUM_NEW=$( md5sum /srv/prometheus/conf/prometheus.yml | awk {'print $1'} );
  if [ x"${PROMETHEUS_CONF_CHECKSUM}" != x"${PROMETHEUS_CONF_CHECKSUM_NEW}" ];
  then
    if [ $( promtool check config /srv/prometheus/conf/prometheus.yml 1>/dev/null 2>&1; echo $? ) -eq 0 ];
    then
      if [ $( curl -XPOST \
        -s \
        -w '%{http_code}' \
        -o /dev/null \
        -m 3 \
        --connect-timeout 3 \
        "${URL}/-/reload";
      PROMETHEUS_CONF_CHECKSUM="${PROMETHEUS_CONF_CHECKSUM_NEW}" ) -eq 200 ];
      then
        echo "{\"ts\":\"$( date -u +'%Y-%m-%dT%H:%M:%SZ' )\",\"level\":\"info\",\"msg\":\"Prometheus reload was successful\"}";
        PROMETHEUS_CONF_CHECKSUM="${PROMETHEUS_CONF_CHECKSUM_NEW}"
      else
        echo "{\"ts\":\"$( date -u +'%Y-%m-%dT%H:%M:%SZ' )\",\"level\":\"error\",\"msg\":\"Prometheus reload was failed!\"}";
      fi;
    else
      echo "{\"ts\":\"$( date -u +'%Y-%m-%dT%H:%M:%SZ' )\",\"level\":\"error\",\"msg\":\"Bad prometheus.yml, please fix it...\"}";
    fi;
  else
    echo "{\"ts\":\"$( date -u +'%Y-%m-%dT%H:%M:%SZ' )\",\"level\":\"info\",\"msg\":\"The prometheus.yml file has not changed\"}";
  fi;
  sleep 15;
done;
