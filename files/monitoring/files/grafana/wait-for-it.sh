#!/bin/bash

URL="http://prometheus:9090";

STATUS_CODE=$( curl -s -o /dev/null -w '%{http_code}' "${URL}/-/ready" );
while [ ${STATUS_CODE} -ne 200 ];
do
  sleep 5;
  echo "{\"ts\":\"$( date -u +'%Y-%m-%dT%H:%M:%SZ' )\",\"level\":\"info\",\"msg\":\"Waiting for Prometheus to start...\"}";
  STATUS_CODE=$( curl -s -o /dev/null -w '%{http_code}' "${URL}/-/ready" );
done;

exec $1
